import React from "react";
import { view } from "@forge/bridge";
import styled from "styled-components";
import Button from "@atlaskit/button";
import CrossIcon from "@atlaskit/icon/glyph/cross";
import { N500 } from "@atlaskit/theme/colors";

function App() {
  const HeadingContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 24px 24px 22px;
  `;

  const H1 = styled.h1`
    font-size: 20px;
    font-weight: 500;
  `;

  return (
    <HeadingContainer>
      <H1>App title here</H1>
      <Button appearance="link" onClick={() => view.close()}>
        <CrossIcon label="Close Modal" primaryColor={N500} />
      </Button>
    </HeadingContainer>
  );
}

export default App;
